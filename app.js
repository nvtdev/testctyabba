// app.js
// const express = require('express');
// const bodyParser = require('body-parser');
// // initialize our express app
// const app = express();
// app.js
const express = require('express');
const bodyParser = require('body-parser');
//import router
const user = require('./routers/user.router'); 
// initialize our express app
const app = express();
// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost:27017/user';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//Use router
app.use('/user', user);



let port = 1234;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});