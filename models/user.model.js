const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {type: String, required: true, max: 100},
    age: {type: Number, required: true},
    gender: {type:String, required:true},
    address:{type:String, required:true}
});


// Export the model
module.exports = mongoose.model('UserInfo', UserSchema);

